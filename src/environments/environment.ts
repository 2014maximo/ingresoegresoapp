// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBb6k_a8Uov1760YW4QRZ9noXfevVq36_c',
    authDomain: 'ingreso-egreso-app-8c4b3.firebaseapp.com',
    databaseURL: 'https://ingreso-egreso-app-8c4b3.firebaseio.com',
    projectId: 'ingreso-egreso-app-8c4b3',
    storageBucket: 'ingreso-egreso-app-8c4b3.appspot.com',
    messagingSenderId: '437733645355',
    appId: '1:437733645355:web:c8d2807914a48363981e6e',
    measurementId: 'G-MCWJ8CR3DJ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
